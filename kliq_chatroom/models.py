from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone as datetime
import uuid

from django.db.models import signals
from django.dispatch import receiver


class Group(models.Model):
    creator = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=255, blank=True)
    description = models.CharField(max_length=255, blank=True)
    dt_created = models.DateTimeField(default=datetime.now, blank=False)
    hashID = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    active = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Group'
        verbose_name_plural = 'Groups'

    def __str__(self):
        if self.name:
            return self.name
        return str(self.hashID)


class GroupInvitees(models.Model):
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    invitee = models.ForeignKey(User, on_delete=models.CASCADE)
    unique_key = models.CharField(max_length=255, unique=True)
    dt_created = models.DateTimeField(default=datetime.now, blank=False)
    can_view_all_history = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Group Invitee'
        verbose_name_plural = 'Group Invitees'

    def save(self, *args, **kwargs):
        self.unique_key = str(self.group.hashID) + '_' + self.invitee.username
        super(GroupInvitees, self).save(*args, **kwargs)  # Call the "real" save() method.

    def __str__(self):
        return str(self.unique_key)


class Message(models.Model):
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    invitee = models.ForeignKey(GroupInvitees, on_delete=models.CASCADE)
    message = models.TextField(max_length=255, blank=False)
    dt_created = models.DateTimeField(default=datetime.now, blank=False)

    class Meta:
        verbose_name = 'Message'
        verbose_name_plural = 'Messages'

    def __str__(self):
        return str(self.message)


class GroupAdmin(models.Model):
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    admin = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Group Admin'
        verbose_name_plural = 'Group Admin'

    def __str__(self):
        return str(self.admin.username)+str(self.group)


class GroupInvite(models.Model):
    admin_inviter = models.ForeignKey(GroupAdmin, on_delete=models.CASCADE)
    invitee_email = models.EmailField(blank=False)
    dt_created = models.DateTimeField(default=datetime.now, blank=False)

    class Meta:
        verbose_name = 'Email Invites'
        verbose_name_plural = 'Email Invites'

    def __str__(self):
        return str(self.invitee_email)


class InviteeStatus(models.Model):
    STATUSES = (
        ('On', 'Online'),
        ('Off', 'Offline'),
        ('Bus', 'Busy'),
    )
    invitee = models.ForeignKey(GroupInvitees, on_delete=models.CASCADE, unique=True)
    status = models.CharField(max_length=3, choices=STATUSES, default="Off")
    dt_created = models.DateTimeField(default=datetime.now, blank=False)

    class Meta:
        verbose_name = 'Invitee Status'
        verbose_name_plural = 'Invitee Statuses'

    def __str__(self):
        return str(self.invitee.unique_key)


class GroupItinerary(models.Model):
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    act_number = models.SmallIntegerField(blank=False, null=False)
    date = models.DateField(blank=True, null=True)
    time_from = models.TimeField(blank=True, null=True)
    time_to = models.TimeField(blank=True, null=True)
    activity = models.TextField(max_length=500, blank=True, null=True)
    location = models.CharField(max_length=255, blank=True, null=True)
    transport_to_location = models.CharField(max_length=255, blank=True, null=True)
    note = models.TextField(max_length=500, blank=True, null=True)
    timestamp = models.DateTimeField(default=datetime.now, blank=False, null=True)

    class Meta:
        unique_together = ('group', 'act_number',)
        verbose_name = 'Group Itinerary'
        verbose_name_plural = 'Group Itinerary'

    def __str__(self):
        return self.group.name


class ActivityInvitee(models.Model):
    act_number = models.SmallIntegerField(default=1, blank=True, null=True)
    invitee = models.ForeignKey(GroupInvitees, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('invitee', 'act_number',)
        verbose_name = 'Activity Invitee Working on'
        verbose_name_plural = 'Activity Invitee Working on'

    def __str__(self):
        return self.invitee.invitee.username


@receiver(signals.post_save, sender=User)
def addGroupInvitee(sender, instance, created, **kwargs):
    print("User created: ", created)
    if instance.email and created:
        invites = GroupInvite.objects.filter(invitee_email=instance.email)
        for invite in invites:
            group = invite.admin_inviter.group
            GroupInvitees.objects.create(group=group, invitee=instance)
