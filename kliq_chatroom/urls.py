from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.chat_area, name='chat_area'),
    url(r'^create-group/$', views.create_group, name='create_group'),
    url(r'^group/(?P<groupID>[^/]+)/msgs/$', views.get_messages, name='msg_fetch'),
    url(r'^group/(?P<groupID>[^/]+)/invitees/statuses$', views.get_statuses, name='status_fetch'),
    url(r'^group/(?P<groupID>[^/]+)/invitees/$', views.get_invitees, name='invitee_fetch'),
    url(r'^email-invite/$', views.email_invite, name='email_invite'),
    url(r'^chat-history/$', views.allow_user_chat_history, name='chat_history'),
    url(r'^group/(?P<groupID>[^/]+)/info/$', views.edit_group_info, name='group_info'),
    url(r'^group/(?P<groupID>[^/]+)/itinerary/$', views.get_group_itinerary, name='group_itinerary'),
]