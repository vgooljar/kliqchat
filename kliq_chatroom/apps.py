from django.apps import AppConfig


class KliqChatroomConfig(AppConfig):
    name = 'kliq_chatroom'
