from channels.generic.websocket import AsyncWebsocketConsumer
from channels.db import database_sync_to_async
import json
from .models import *
from django.utils import timezone as datetime

INVITEE_ACTIVITY_IDER = '/'
FIELD_KEYWORDS = {"date":"date", "from":"time_from", "to":"time_to", "activity":"activity", "location":"location",
                  "transport":"transport_to_location", "note":"note"}
ACTION_KEYWORDS = ["remove", "delete", "invite"]


class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name
        self.user = self.scope["user"]

        is_invitee = await user_check(self.user, self.room_name)
        if is_invitee:
            # Join room group
            await self.channel_layer.group_add(
                self.room_group_name,
                self.channel_name
            )

            await self.accept()

            await write_status(self.user, self.room_name, "On")

            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'chat_message',
                    'message': '{"user_event":{"username":"'+str(self.user)+'","activity":"Online"}}',
                }
            )

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

        #check if user is still in channel (through a duplicate connection)
        #if no more existing channel from user, then put them as offline?
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': '{"user_event":{"username":"'+str(self.user)+'","activity":"Offline"}}',
            }
        )

        await write_status(self.user, self.room_name, "Off")

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        if '--typing--' in message:
            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'chat_message',
                    'message': '{"user_event":{"username":"' + str(self.user) + '","activity":"Typing"}}',
                }
            )

        elif message is not '':
            if message.split(' ', 1)[0].lower() in FIELD_KEYWORDS.keys():
                if len(message.split(' ', 1)) > 1:
                    await itinerary_processor(self.user, self.room_name, message)

            elif message[0] == INVITEE_ACTIVITY_IDER:
                await chat_action(self.user, self.room_name, message)

            ##################################################################################################

            else:
                await write_message(self.user, self.room_name, message)

                formatted_msg = '{"msg":{"username":"'+str(self.user)+'", "message":"'+message+'", "dt_created":"' + \
                                str(datetime.datetime.now().isoformat())+'"}}'

                await self.channel_layer.group_send(
                    self.room_group_name,
                    {
                        'type': 'chat_message',
                        'message': formatted_msg
                    }
                )

    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']
        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message
        }))


@database_sync_to_async
def write_message(iuser, iroom, imsg):
    group = Group.objects.get(hashID=iroom)
    invitee = GroupInvitees.objects.get(group=group, invitee=iuser)
    return Message.objects.create(group=group, invitee=invitee, message=imsg)


@database_sync_to_async
def user_check(iuser, iroom):
    group = Group.objects.get(hashID=iroom)
    is_group_invitee = GroupInvitees.objects.filter(group=group, invitee=iuser).exists()
    return is_group_invitee


@database_sync_to_async
def write_status(iuser, iroom, istatus):
    group = Group.objects.get(hashID=iroom)
    invitee = GroupInvitees.objects.get(group=group, invitee=iuser)
    p, created = InviteeStatus.objects.get_or_create(invitee=invitee)
    p.status = status=istatus
    p.save()


@database_sync_to_async
def itinerary_processor(iuser, iroom, imsg):
    cmd = imsg.split(' ', 1)[0].lower()
    msg = imsg.split(' ', 1)[1]
    group = Group.objects.get(hashID=iroom)
    invitee = GroupInvitees.objects.get(group=group, invitee=iuser)

    if cmd in FIELD_KEYWORDS.keys():
        # depending on user, we get the activity they last selected to work on. We get that activity, and save new changed field
        working_on = ActivityInvitee.objects.get(invitee=invitee)
        group_activity, created = GroupItinerary.objects.get_or_create(group=group, act_number=working_on.act_number)
        setattr(group_activity, FIELD_KEYWORDS[cmd], msg)
        group_activity.save()


@database_sync_to_async
def chat_action(iuser, iroom, imsg):
    group = Group.objects.get(hashID=iroom)
    invitee = GroupInvitees.objects.get(group=group, invitee=iuser)
    cmd = imsg[1::].strip().split(' ', 1)
    print(cmd[0])
    if cmd[0].lower() in ACTION_KEYWORDS:
        if cmd is "remove" or cmd is "delete":
            working_on = ActivityInvitee.objects.get(invitee=invitee)
            GroupItinerary.objects.filter(act_number=working_on.act_number).delete()
    elif cmd[0].isdigit():
        act_num = int(cmd[0])
        working_on, created = ActivityInvitee.objects.get_or_create(invitee=invitee)
        working_on.act_number = act_num
        working_on.save()
