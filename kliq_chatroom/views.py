from django.shortcuts import render
from django.utils.safestring import mark_safe
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from kliq_chatroom.models import *
import json
import uuid
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import F
from .emailer import *
from collections import defaultdict
from django.forms.models import model_to_dict


def is_group_admin(user, group):
    return GroupAdmin.objects.filter(group=group, admin=user).exists()


def chat_area(request):
    if request.user.is_authenticated:
        groups_affiliated = GroupInvitees.objects.filter(invitee=request.user, group__active=True).values_list(
            'group__hashID', 'group__name')
        print(groups_affiliated)
        groups_joined = {}
        for grp in groups_affiliated:
            if grp[1]:
                groups_joined[str(grp[0])] = grp[1]
            else:
                groups_joined[str(grp[0])] = str(grp[0])
        print(groups_joined)
        return render(request, 'kliq_chatroom/room.html', {'groups_joined': json.dumps(groups_joined)})
    else:
        return redirect('/')


@login_required
def create_group(request):
    group = Group.objects.create(creator=request.user, name="", description="")
    invitee = GroupInvitees.objects.create(group=group, invitee=request.user)
    admin = GroupAdmin.objects.create(group=group, admin=request.user)
    return HttpResponse(str(group.hashID))


@login_required
def get_messages(request, groupID):
    groupID = uuid.UUID(groupID)
    if request.user.is_authenticated:
        user_invite = GroupInvitees.objects.get(invitee=request.user, group__hashID=groupID)
        messages = ''
        if user_invite.can_view_all_history:
            messages = Message.objects.annotate(username=F('invitee__invitee__username')).filter(
                group__hashID=groupID).values('username', 'message', 'dt_created')
        else:
            messages = Message.objects.annotate(username=F('invitee__invitee__username')).filter(group__hashID=groupID,
                                                                                                 dt_created__gte=user_invite.dt_created).values(
                'username', 'message', 'dt_created')
        messages_json = json.dumps(list(messages), cls=DjangoJSONEncoder)
        return JsonResponse(messages_json, safe=False)

    return HttpResponse('')


@login_required
def get_invitees(request, groupID):
    groupID = uuid.UUID(groupID)
    if request.user.is_authenticated:
        invitees = GroupInvitees.objects.filter(group__hashID=groupID).values('invitee__email', 'invitee__username')
        invitees_json = json.dumps(list(invitees), cls=DjangoJSONEncoder)
        return JsonResponse(invitees_json, safe=False)

    return HttpResponse('')


@login_required
def email_invite(request):
    # IF EMAIL IS IN SYSTEM, send email to alert user that they have been invited to a group, and add to GroupInvite table
    if request.user.is_authenticated:
        if request.method == 'POST':
            email = request.POST['email']
            group = request.POST['group']
            group = Group.objects.get(hashID=group)
            groupadmin = GroupAdmin.objects.get(group=group, admin=request.user)

            if email and group:
                if not GroupInvite.objects.filter(admin_inviter=groupadmin, invitee_email=email).exists():
                    GroupInvite.objects.create(admin_inviter=groupadmin, invitee_email=email)
                if User.objects.filter(email=email).exists():
                    if not GroupInvitees.objects.filter(group=group, invitee=User.objects.get(email=email)).exists():
                        GroupInvitees.objects.create(group=group, invitee=User.objects.get(email=email))
                try:
                    send_email(email, "GoKliq Group Invite",
                               "You have been Invited to a group named: " + str(group), False)
                    return HttpResponse('Email Sent')
                except ValueError:
                    return HttpResponse('Error sending email! - ' + ValueError)
                return HttpResponse("Error at DB level")
            else:
                return HttpResponse("Parameter Fail")


@login_required
def allow_user_chat_history(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            email = request.POST['email']
            group = request.POST['group']
            allow = bool(request.POST['allow'])

            group = Group.objects.get(hashID=group)
            user_obj = User.objects.get(email=email)

            # check if request user is an admin
            if GroupAdmin.objects.filter(group=group, admin=request.user).exists():
                if allow:
                    if email and group:
                        invitee = GroupInvitees.objects.get(group=group, invitee=user_obj)
                        invitee.can_view_all_history = True
                        invitee.save()
                        return HttpResponse('OK')
                else:
                    if email and group:
                        invitee = GroupInvitees.objects.get(group=group, invitee=user_obj)
                        invitee.can_view_all_history = False
                        invitee.save()
                        return HttpResponse('OK')
            else:
                return HttpResponse("User needs to be an admin to allow others to permanently view chat history")


@login_required
def get_statuses(request, groupID):
    group_statuses = InviteeStatus.objects.filter(invitee__group__hashID=groupID)
    response = {}
    for obj in group_statuses:
        response[obj.invitee.invitee.username] = obj.get_status_display()

    group_statuses_json = json.dumps(response, cls=DjangoJSONEncoder)
    return JsonResponse(group_statuses_json, safe=False)


@login_required
def edit_group_info(request, groupID):
    groupID = uuid.UUID(groupID)
    if request.method == 'POST':
        name = request.POST['group_name']
        group = Group.objects.get(hashID=groupID)

        if is_group_admin(request.user, group):
            group.name = name
            group.save()
            return HttpResponse('Ok')

        return HttpResponse('Fail')


@login_required
def get_group_itinerary(request, groupID):
    groupID = uuid.UUID(groupID)
    invitee = GroupInvitees.objects.get(group__hashID=groupID, invitee=request.user)

    dates = GroupItinerary.objects.filter(group__hashID=groupID).values('date').distinct()
    group_itin_dict = {}

    for date in dates:
        sdate = date['date']
        group_itin_dict[sdate] = GroupItinerary.objects.filter(group__hashID=groupID, date=sdate).order_by('time_from')

    json_dict = defaultdict(list)
    for date,objlist in group_itin_dict.items():
        print(date)
        for obj in objlist:
            json_dict[str(date)].append({
                'from':str(obj.time_from),
                'to':str(obj.time_to),
                'activity':obj.activity,
                'activity_num':obj.act_number,
                'location':obj.location,
                'note':obj.note,
            })

    return HttpResponse(json.dumps(json_dict, sort_keys=True))
