from django.contrib import admin
from .models import *


class GroupDisplay(admin.ModelAdmin):
    list_display = ('creator', 'name', 'description', 'hashID', 'dt_created','active')


class InviteeDisplay(admin.ModelAdmin):
    list_display = ('group', 'invitee')


class MessageDisplay(admin.ModelAdmin):
    list_display = ('invitee', 'group', 'message', 'dt_created')


class GroupInviteDisplay(admin.ModelAdmin):
    list_display = ('invitee_email', 'admin_inviter', 'dt_created')


class GroupAdminDisplay(admin.ModelAdmin):
    list_display = ('group', 'admin')


class InviteeStatusDisplay(admin.ModelAdmin):
    list_display = ('invitee', 'status', 'dt_created')


class GroupItineraryDisplay(admin.ModelAdmin):
    list_display = ('group', 'date', 'time_from', 'time_to', 'activity', 'location', 'transport_to_location',
                    'note', 'timestamp')


admin.site.register(Group, GroupDisplay)
admin.site.register(GroupInvitees, InviteeDisplay)
admin.site.register(Message, MessageDisplay)
admin.site.register(GroupAdmin, GroupAdminDisplay)
admin.site.register(GroupInvite, GroupInviteDisplay)
admin.site.register(InviteeStatus, InviteeStatusDisplay)
admin.site.register(GroupItinerary, GroupItineraryDisplay)
admin.site.register(ActivityInvitee)