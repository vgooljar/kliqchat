from django.contrib import admin
from .models import *


class VerificationDisplay(admin.ModelAdmin):
    list_display = ('email', 'code', 'dt_created')


admin.site.register(VerificationCode, VerificationDisplay)
