from background_task import background
from .models import *


@background(schedule=60*60*24)  # schedule 1 day from now
def delete_verification_code(email, code):
    VerificationCode.objects.get(email=email, code=code).delete()
