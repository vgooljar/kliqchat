from django.apps import AppConfig


class KliqUsersConfig(AppConfig):
    name = 'kliq_users'
