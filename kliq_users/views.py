from django.shortcuts import render, render_to_response, redirect, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.sessions.models import Session
from django.contrib.auth import authenticate, login as auth_login
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.password_validation import validate_password

from django.contrib.auth.models import User
from social_django.models import UserSocialAuth

from .sms import *
from .models import *
from .emailer import *
from .tasks import *
import random
import json
from kliq_chatroom.models import *
from .forms import *


BASE_URL = '/'


def homepage(request):
    if request.user.is_authenticated:
        return redirect('/chat/')
    else:
        form = UserForm()
        return render(request, 'kliq_chatroom/index.html', {'form': form})


@login_required(login_url='/')
def userhome(request):
    return render_to_response('home.html')


def api_users(request):
    if request.method == 'GET':
        json_users = []
        for user in User.objects.filter(is_staff=False):
            login_type = UserSocialAuth.objects.filter(user_id=user.id).values('provider')
            if not login_type:
                login_type = 'Site Registered'
            else:
                print(login_type)
                login_type = login_type.first()['provider']

            json_obj = dict(
                first_name=user.first_name,
                last_name=user.last_name,
                email_address=user.email,
                login_type=login_type
            )
            json_users.append(json_obj)
        return HttpResponse(json.dumps(json_users))


def registration(request):
    if request.method == 'POST':
        code = request.POST['email_verification_code']
        userform = UserForm(request.POST)

        if userform.is_valid():
            password = userform.cleaned_data["password"]
            validate_password(password)
            print("passed password check")
            if VerificationCode.objects.filter(email=userform.cleaned_data["email"], code=code).exists():
                userform.save()
                return HttpResponse("OK")
        else:
            HttpResponse("Userform is not valid")


def login(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            auth_login(request, user)
            return HttpResponse('/chat/')
            # requestUser = UserProfile.objects.get(user=user)
            # if requestUser.isVerified:
            #     auth_login(request, user)
            #     return redirect("/")
            # else:
            #     return HttpResponse('User is not verified via email')
        else:
            return HttpResponse('User is disabled.')
    else:
        return HttpResponse('Username does not exist.')


@login_required
def logout(request):
    auth_logout(request)
    return redirect(BASE_URL)


def verification_code(request):
    if not request.user.is_authenticated:
        if request.method == 'POST':
            email = request.POST['email']
            if email:
                code = ''.join(random.choice('23456789zxasdfghjkcvbnmqwertyup') for i in range(8))
                obj = VerificationCode.objects.create(email=email, code=code)
                delete_verification_code(email, code)
                if obj:
                    try:
                        send_email(email, "GoKliq Verification Email",
                                   "Your GoKliq verification code is: <strong>" + code + "</strong>", False)
                        return HttpResponse('Email Sent')
                    except ValueError:
                        return HttpResponse('Error sending email! - ' + ValueError)
                return HttpResponse("Error at DB level")
    return HttpResponse("Failed")

