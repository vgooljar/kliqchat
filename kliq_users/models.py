from django.db import models
from django.utils import timezone as datetime


class VerificationCode(models.Model):
    email = models.EmailField(blank=False)
    code = models.CharField(max_length=8, blank=False)
    dt_created = models.DateTimeField(default=datetime.now, blank=False)

    class Meta:
        verbose_name = 'Verification Code'
        verbose_name_plural = 'Verification Codes'

    def __str__(self):
        return self.email
