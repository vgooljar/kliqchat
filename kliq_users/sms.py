from twilio.rest import Client
from pprint import pprint  # debug

from phonenumbers import carrier  # pip install phonenumbers
import phonenumbers


account_sid = "ACd696ee1347da720061f2409dc55b0b49" # Your Account SID from www.twilio.com/console
auth_token  = "b1e71b1e843ff0412587864d9e6f794d"  # Your Auth Token from www.twilio.com/console

client = Client(account_sid, auth_token)


def textmsg(phonenumber, msg, countrycode):
    number = phonenumbers.parse(phonenumber, countrycode)
    if carrier.name_for_number(number, 'en') != '':
        message = client.messages.create(body=msg, to=phonenumber, from_="+13177933648")
        pprint(message.sid)
        return True
    else: return False


def verification_textmsg(number, code):
    message = client.messages.create(body="Your Xeemit verification code is: "+code,
            to=number,    # Replace with your phone number
            from_="+13177933648")  # Replace with your Twilio number
    pprint(message.sid)
